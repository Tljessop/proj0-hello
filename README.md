# Proj0-Hello
-------------
## Discription
  This was starter project for CIS 322 taught by Prof. Ram Durairajan. With the objective of ensuring that students can fork class project repos.
## Function of Program
  To print a simple message to the terminal. In order for this program to function properly you must have a file named credentials.ini in the same dictory as hello.py and said .ini file must define a constant of message.
  
  The syntax of said constant definition is as follows

  ```
    message=What you would like to print to the terminal
  ```
## Useage
  To run the program first ensure that you in the PROJ0-HELLO directory then enter the following command
  ```bash
    make run
  ```
## Authors
 ### Original files by Prof. Ram Durairajan
 ### Modified by Thomas Lynn Jessop
## Contact Information
  Thomas Jessop
  tjessop2@uoregon.edu